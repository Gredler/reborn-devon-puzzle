# Reborn Devon puzzle
This is just a little program to figure out the solution to a puzzle in the [Pokemon Reborn fangame](http://www.rebornevo.com/).

## Rules
- Each Line on either 3x3 grid must add up to 15
- Lines count horizontally, vertically and diagonally
- Except the center, no 2 numbers can be in the same position on both sides

### Possible moves:
- Left Grid: up / down for Columns 1-3
- Right Grid: up / down for Columns 1-3
- Both: right / left for Rows 1-3

(in total that's 18 possible moves)

## Starting Grid
<img src="https://i.imgur.com/tSKm4kc.png" alt="The start grid"><br><br>

## Rewards
After solving it, you get the following rewards:

- Darkinium-Z
- TM 24: Thunderbolt
- Gardevoirite
- The following screen to be used as a password in Agate City

<img src="https://i.imgur.com/O4avAfd.png" alt="The password"><br><br>

## Known solutions
- 41: [RIGHT_C1_DOWN, RIGHT_C2_UP, ROW1_LEFT, LEFT_C3_UP, ROW1_LEFT, LEFT_C1_UP, ROW1_RIGHT, LEFT_C2_UP, RIGHT_C3_DOWN, RIGHT_C2_DOWN, ROW1_LEFT, ROW3_LEFT, LEFT_C3_DOWN, RIGHT_C3_UP, RIGHT_C3_UP, LEFT_C1_UP, LEFT_C1_UP, LEFT_C2_UP, LEFT_C3_DOWN, RIGHT_C1_UP, LEFT_C2_UP, RIGHT_C3_UP, LEFT_C3_DOWN, ROW3_LEFT, RIGHT_C3_UP, ROW3_RIGHT, LEFT_C2_UP, LEFT_C3_DOWN, LEFT_C2_UP, LEFT_C2_DOWN, LEFT_C3_UP, LEFT_C1_UP, RIGHT_C3_DOWN, RIGHT_C1_UP, ROW3_RIGHT, ROW1_LEFT, LEFT_C2_UP, RIGHT_C3_DOWN, ROW1_LEFT, RIGHT_C3_DOWN, RIGHT_C1_UP]

