import puzzle.Puzzle;

/**
 * the main class
 */
public class Main {

    /**
     * main class creating a new Puzzle object and calling the start method of that object
     * @param args
     */
    public static void main(String[] args) {
        Puzzle p = new Puzzle();
        p.start();
    }
}
