package puzzle;

/**
 * moves Enum containing all the possible moves
 * numbers are there purely for readability and to
 * quickly check what index coincides with which move
 */
public enum Moves {
    LEFT_C1_UP(0),
    LEFT_C1_DOWN(1),
    LEFT_C2_UP(2),
    LEFT_C2_DOWN(3),
    LEFT_C3_UP(4),
    LEFT_C3_DOWN(5),
    RIGHT_C1_UP(6),
    RIGHT_C1_DOWN(7),
    RIGHT_C2_UP(8),
    RIGHT_C2_DOWN(9),
    RIGHT_C3_UP(10),
    RIGHT_C3_DOWN(11),
    ROW1_RIGHT(12),
    ROW1_LEFT(13),
    ROW2_RIGHT(14),
    ROW2_LEFT(15),
    ROW3_RIGHT(16),
    ROW3_LEFT(17);

    private int index;

    Moves(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
