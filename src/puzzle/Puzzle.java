package puzzle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * The actual puzzle
 */
public class Puzzle {

    /**
     * starting positions of the left grid
     */
    private static final byte[][] LEFT_START = {
            {4, 9, 6},
            {5, 8, 3},
            {2, 4, 1}
    };

    /**
     * starting positions of the right grid
     */
    private static final byte[][] RIGHT_START = {
            {1, 2, 5},
            {7, 7, 9},
            {6, 3, 8}
    };

    /**
     * all the moves inside the Moves enum
     */
    private static final Moves[] MOVES = Moves.values();

    /**
     * the current values of the left grid
     */
    private byte[][] current_left;

    /**
     * the current values of the right grid
     */
    private byte[][] current_right;

    /**
     * a list of successful move squences (as a list)
     */
    private List<List<Moves>> succ_moves;

    /**
     * the current sequence of moves being executed
     */
    // current: [2, 10, 11, 4, 12, 10, 15, 8, 1, 14]
    private List<Integer> moves_sequence;

    /**
     * temporary grid
     */
    private byte[][] tempGrid;

    /**
     * temporary row
     */
    private byte[] tempRow;


    /**
     * constructor
     * initializes all the fields and adds 0 to the moves_sequence as a starting point
     */
    public Puzzle() {
        succ_moves = new ArrayList<>();
        moves_sequence = new ArrayList<>();
//        moves_sequence.add(0);

        current_left = new byte[3][3];
        current_right = new byte[3][3];
        tempGrid = new byte[3][3];
        tempRow = new byte[6];

        moves_sequence.add(2);
        moves_sequence.add(10);
        moves_sequence.add(11);
        moves_sequence.add(4);
        moves_sequence.add(12);
        moves_sequence.add(10);
        moves_sequence.add(15);
        moves_sequence.add(8);
        moves_sequence.add(1);
        moves_sequence.add(14);

        resetGrids();
    }

    /**
     * the starting point of the puzzle
     */
    public void start() {
//        for (int x = 0; x < LEFT_START.length; x++) {
//            for (int y = 0; y < LEFT_START[x].length; y++) {
//                System.out.println(LEFT_START[x][y]);
//            }
//        }


        boolean finished = false;
        long printCounter = 0;

        // ##############################################################################
        // sequentially go through every possible combination to
        // figure out the most efficient series of moves
        while (!finished) {
            for (Integer move : moves_sequence) {
                doMove(MOVES[move]);
            }

            if (printCounter % 10000000 == 0) {
                System.out.println(moves_sequence);
            }

            // overflow reset
            if (printCounter < 0) {
                printCounter = 0;
            }

            finished = checkResult();
            incrementSequence();
            resetGrids();

            printCounter++;
        }

        // ##############################################################################
        // randomly do a series of 50 moves and check the result
        // to hopefully get some kind of result
//        Random random = new Random();
//        List<Moves> usedMoves = new ArrayList<>();
//        int counter = 0;
//        while (!finished) {
//            printCounter++;
//            if (counter != 50) {
//                Moves m = MOVES[random.nextInt(18)];
//                doMove(m);
//                usedMoves.add(m);
//                counter++;
//                finished = checkResult();
//            } else {
//                finished = checkResult();
//                counter = 0;
//                resetGrids();
//                usedMoves.clear();
//            }
//
//            if (!finished) {
//                if (printCounter % 100000000 == 0) {
//                    System.out.println("Still going");
//                }
//
//                // overflow reset
//                if (printCounter < 0) {
//                    printCounter = 0;
//                }
//            }
//        }
//
//        System.out.println(usedMoves);
//        System.out.println(usedMoves.size());
//        System.out.println(Arrays.deepToString(current_left));
//        System.out.println(Arrays.deepToString(current_right));

        // ##############################################################################
        // just random as a check of solvability and to see if the program actually works
//        Random random = new Random();
//        long counter = 0;
//        while (!finished) {
//            Moves m = MOVES[random.nextInt(18)];
//            doMove(m);
//            counter++;
//            finished = checkResult();
//
//            if (counter % 100000000 == 0 && !finished) {
//                System.out.println(counter);
//                System.out.println(Arrays.deepToString(current_left));
//                System.out.println(Arrays.deepToString(current_right));
//            }
//        }
//
//        System.out.println("Number of used moves: " + counter);
//
//        System.out.println(Arrays.deepToString(current_left));
//        System.out.println(Arrays.deepToString(current_right));
        // ##############################################################################

        System.out.println("The successful sequence:");
        System.out.println(succ_moves);
    }

    /**
     * executes the given move
     *
     * @param move
     */
    private void doMove(Moves move) {
        switch (move) {
            case LEFT_C1_UP:
                moveColumnUp(true, 0);
                break;
            case LEFT_C1_DOWN:
                moveColumnDown(true, 0);
                break;
            case LEFT_C2_UP:
                moveColumnUp(true, 1);
                break;
            case LEFT_C2_DOWN:
                moveColumnDown(true, 1);
                break;
            case LEFT_C3_UP:
                moveColumnUp(true, 2);
                break;
            case LEFT_C3_DOWN:
                moveColumnDown(true, 2);
                break;
            case RIGHT_C1_UP:
                moveColumnUp(false, 0);
                break;
            case RIGHT_C1_DOWN:
                moveColumnDown(false, 0);
                break;
            case RIGHT_C2_UP:
                moveColumnUp(false, 1);
                break;
            case RIGHT_C2_DOWN:
                moveColumnDown(false, 1);
                break;
            case RIGHT_C3_UP:
                moveColumnUp(false, 2);
                break;
            case RIGHT_C3_DOWN:
                moveColumnDown(false, 2);
                break;
            case ROW1_RIGHT:
                moveRow(false, 0);
                break;
            case ROW1_LEFT:
                moveRow(true, 0);
                break;
            case ROW2_RIGHT:
                moveRow(false, 1);
                break;
            case ROW2_LEFT:
                moveRow(true, 1);
                break;
            case ROW3_RIGHT:
                moveRow(false, 2);
                break;
            case ROW3_LEFT:
                moveRow(true, 2);
                break;
            default:
                System.err.println("WHOOPS, SOMETHING WENT WRONG!");
                break;
        }
    }

    /**
     * moves the given column up by calling the moveColumn method with the correct values
     *
     * @param left   boolean value to determine if we are moving left or right (true if left)
     * @param column the column to move
     */
    private void moveColumnUp(boolean left, int column) {
        moveColumn(left, column, 1, 2, 0);
    }

    /**
     * moves the given column down by calling the moveColumn method with the correct values
     *
     * @param left   true if left, else false
     * @param column the column to move
     */
    private void moveColumnDown(boolean left, int column) {
        moveColumn(left, column, 2, 0, 1);
    }

    /**
     * moves the column
     *
     * @param left   true if left, else false
     * @param column the column
     * @param pos0   the new position of the original 0 position value
     * @param pos1   the new position of the original 1 position value
     * @param pos2   the new position of the original 2 position value
     */
    private void moveColumn(boolean left, int column, int pos0, int pos1, int pos2) {
        byte[][] temp;
        if (left) {
            // to not use a pointer
            temp = getTempGrid(current_left);

            current_left[0][column] = temp[pos0][column];
            current_left[1][column] = temp[pos1][column];
            current_left[2][column] = temp[pos2][column];
        } else {
            temp = getTempGrid(current_right);

            current_right[0][column] = temp[pos0][column];
            current_right[1][column] = temp[pos1][column];
            current_right[2][column] = temp[pos2][column];
        }
    }

    /**
     * sets the values of the tempGrid field to the values of the given base grid and returns it
     *
     * @param base the grid whose values to write into the tempGrid
     * @return the tempGrid
     */
    private byte[][] getTempGrid(byte[][] base) {
        for (int x = 0; x < tempGrid.length; x++) {
            for (int y = 0; y < tempGrid[x].length; y++) {
                tempGrid[x][y] = base[x][y];
            }
        }

        return tempGrid;
    }

    /**
     * writes the values of the given row of the left and right grid into the
     * tempRow variable and returns it
     *
     * @param row the row (1, 2 or 3)
     * @return the tempRow
     */
    private byte[] getTempRow(int row) {

        tempRow[0] = current_left[row][0];
        tempRow[1] = current_left[row][1];
        tempRow[2] = current_left[row][2];
        tempRow[3] = current_right[row][0];
        tempRow[4] = current_right[row][1];
        tempRow[5] = current_right[row][2];

        return tempRow;
    }

    /**
     * moves the row of the grid either right or left
     *
     * @param left true if left, else false
     * @param row  the row to move (0, 1 or 2)
     */
    private void moveRow(boolean left, int row) {
        byte[] temp = getTempRow(row);

        if (left) {
            current_left[row][0] = temp[1];
            current_left[row][1] = temp[2];
            current_left[row][2] = temp[3];

            current_right[row][0] = temp[4];
            current_right[row][1] = temp[5];
            current_right[row][2] = temp[0];
        } else {
            current_left[row][0] = temp[5];
            current_left[row][1] = temp[0];
            current_left[row][2] = temp[1];

            current_right[row][0] = temp[2];
            current_right[row][1] = temp[3];
            current_right[row][2] = temp[4];
        }
    }

    /**
     * checks if the values inside the current_right and current_left fields
     * are correct
     *
     * @return true the result is correct, else false
     */
    private boolean checkResult() {
        boolean result = checkAllPositions() && (checkGrid(current_left) && checkGrid(current_right));

        if (result) {
            System.out.println("WOW");
            System.out.println(moves_sequence);

            List<Moves> amazing_moves = new ArrayList<>();
            for (int m = 0; m < moves_sequence.size(); m++) {
                amazing_moves.add(MOVES[moves_sequence.get(m)]);
            }
            succ_moves.add(amazing_moves);
            System.out.println(amazing_moves);
        } else {
//            System.out.println("FAIL");
        }

        return result;
    }

    /**
     * checks all the rows and diagonals of the given grid
     *
     * @param grid the grid
     * @return boolean
     */
    private boolean checkGrid(byte[][] grid) {
        return checkAllColumns(grid) && checkAllRows(grid) && checkDiagonals(grid);
    }

    /**
     * checks all the rows of the given grid
     *
     * @param grid the grid
     * @return boolean
     */
    private boolean checkAllRows(byte[][] grid) {
        return (checkRow(0, grid) && checkRow(1, grid) && checkRow(2, grid));
    }

    /**
     * checks the given row of the given grid
     *
     * @param row  the row (0, 1 or 2)
     * @param grid the grid
     * @return boolean
     */
    private boolean checkRow(int row, byte[][] grid) {
        return (grid[row][0] + grid[row][1] + grid[row][2]) == 15;
    }

    /**
     * checks all the columns of the given grid
     *
     * @param grid the grid
     * @return boolean
     */
    private boolean checkAllColumns(byte[][] grid) {
        return (checkColumn(0, grid) && checkColumn(1, grid) && checkColumn(2, grid));
    }

    /**
     * checks the given column of the given grid
     *
     * @param col  the column (0, 1 or 2)
     * @param grid the grid
     * @return boolean
     */
    private boolean checkColumn(int col, byte[][] grid) {
        return (grid[0][col] + grid[1][col] + grid[2][col]) == 15;
    }

    /**
     * checks both the diagonals of the given grid
     *
     * @param grid the grid
     * @return boolean
     */
    private boolean checkDiagonals(byte[][] grid) {
        return ((grid[0][0] + grid[1][1] + grid[2][2]) == 15
                && (grid[0][2] + grid[1][1] + grid[2][0]) == 15);
    }

    /**
     * checks if no numbers are in the same position on both grids
     * (not counting the center, where having equal numbers is accepted)
     *
     * @return boolean
     */
    private boolean checkAllPositions() {
        boolean res = true;
        for (int x = 0; x < LEFT_START.length; x++) {
            for (int y = 0; y < LEFT_START[x].length; y++) {
                res = res && checkPosition(x, y);
            }
        }

        return res;
    }

    /**
     * checks the given position
     *
     * @param posX the x value of the position
     * @param posY the y value of the position
     * @return boolean
     */
    private boolean checkPosition(int posX, int posY) {
        if (posX != 1 && posY != 1) {
            return (current_left[posX][posY] != current_right[posX][posY]);
        }
        return true;
    }

    /**
     * increments the moves_sequence
     * adds a new 0 if all the values inside the list are 17
     */
    private void incrementSequence() {
        Integer lastVal = moves_sequence.get(moves_sequence.size() - 1);
        moves_sequence.set(moves_sequence.size() - 1, lastVal + 1);

        for (int i = moves_sequence.size() - 1; i >= 0; i--) {
            if (moves_sequence.get(i) == 18 && i != 0) {
                moves_sequence.set(i - 1, moves_sequence.get(i - 1) + 1);
                moves_sequence.set(i, 0);
            }
        }

        if (moves_sequence.get(0) == 18) {
            moves_sequence.set(0, 0);
            moves_sequence.add(0);
        }
    }


    /**
     * resets the current_right and current_left fields
     * to their original values
     */
    private void resetGrids() {
        for (int x = 0; x < LEFT_START.length; x++) {
            for (int y = 0; y < LEFT_START[x].length; y++) {
                current_left[x][y] = LEFT_START[x][y];
                current_right[x][y] = RIGHT_START[x][y];
            }
        }
    }
}
